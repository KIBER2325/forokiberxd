import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Editor, Toolbar } from 'ngx-editor';
// import { title } from 'process';
import { Post2Service } from '../services/post2.service';
import { PostI} from './post.interface';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.css']
})
export class CategoriesComponent implements OnInit { 
  postForm: FormGroup;
  posts: PostI[];
  error = null;




  html = '';
  editor: Editor;
  toolbar: Toolbar = [
    ['bold', 'italic'],
    ['underline', 'strike'],
    ['code', 'blockquote'],
    ['ordered_list', 'bullet_list'],
    [{ heading: ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'] }],
    ['link', 'image'],
    ['text_color', 'background_color'],
    ['align_left', 'align_center', 'align_right', 'align_justify'],
  ];
constructor(private postService: Post2Service){
  this.editor = new Editor();
}
ngOnInit(): void {
  this.postForm = new FormGroup({
    titulo: new FormControl(null, Validators.required),
    content: new FormControl('', Validators.required),
    
  });
  this.getPosts();
}


  getPosts() {
    this.postService.fetchPosts().subscribe(
      (response) => {
        this.posts = response;
      },
      (error) => {
        console.log(error);
        this.error = error.message;
      }
    );
  }


  Guardar():void {
    console.log(this.html);
      let valores:PostI ={
      titulo:this.postForm.get('titulo').value,
      content:this.postForm.get(this.html).value     
    }
       
    console.log(valores);
  }

  

  onCreatePost() {
    let postData: PostI = this.postForm.value;
    console.log(postData);
    
    this.postService.createPost(postData).subscribe((response) => {
      console.log(response);
      this.getPosts();
    });
  }

  onClearPosts(event: Event) {
    event.preventDefault();
    this.postService.clearPosts();
    this.posts = [];
  }
}
