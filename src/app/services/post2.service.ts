import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, tap } from 'rxjs';
import { PostI } from '../categories/post.interface';
import { AuthService } from './auth.service';

@Injectable({
  providedIn: 'root'
})
export class Post2Service {

  constructor(private http: HttpClient, private authService: AuthService) { }
  
  fetchPosts() {
    return this.http
      .get<{ [key: string]: PostI }>(
        `https://forok-14cd1-default-rtdb.firebaseio.com/posts2.json`
      )
      .pipe(
        map((response) => {
          let posts: PostI[] = [];
          for (let key in response) {
            posts.push({ ...response[key], key });
          }
          return posts;
        })
      );
  }

  createPost(postData: PostI) {
    return this.http.post<{ name: string }>(
      'https://forok-14cd1-default-rtdb.firebaseio.com/posts2.json',
      postData,
      {
        headers: new HttpHeaders({
          'custom-header': 'post Leela',
        }),
        observe: 'body',
      }
    );
  }
  
  clearPosts() {
    this.http
      .delete('https://forok-14cd1-default-rtdb.firebaseio.com/posts2.json', {
        observe: 'events',
        responseType: 'text',
      })
      .pipe(
        tap((response) => {
          if (response.type === HttpEventType.Sent) {
            console.log('request sent');
          }

          if (response.type === HttpEventType.Response) {
            console.log(response);
          }
        })
      )
      .subscribe((response) => {
        //console.log(response);
      });
  }
}
